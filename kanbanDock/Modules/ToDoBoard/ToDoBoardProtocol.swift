//
//  ToDoBoardProtocol.swift
//  kanbanDock
//
//  Created by Marina on 27.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

protocol DoDoBoardConfiguratorProtocol: class {
    func configure(with viewController: ToDoBoardVC)
}

protocol ToDoBoardViewInput: class {
    var presenter: ToDoBoardViewOutput? {get set}
    
    // ToDOBoardPresenter -> ToDoBoardView
    func setupInitialView()
    func showToDoTasks(with tasks:[TasksModel])
}

protocol ToDoBoardViewOutput: class {
    // ToDoBoardView -> ToDoBoardPresenter
    func viewDidLoad()
    func reloadData()
    func addButtonClicked()
    func saveNewTask(name: String)
    func deleteTask(model: TasksModel)
    func moveTaskToDoingList(task: TasksModel)
    
}

protocol ToDoBoardInteractorInput: class {
    var presenter: ToDoBoardInteractorOutput? { get set }
    //var localDatamanager: ToDoBoardDataManagerInput? { get set }
    
    //ToDoBoardPresenter -> ToDoboardInteractor
    func retrieveToDoList()
    func addNewTaskinTodoList(name: String)
    func deleteTask(id: String)
    func moveTaskToDoingList(id: String)
}

protocol ToDoBoardInteractorOutput: class {
    
    //ToDoboardInteractor -> ToDoBoardPresenter
    func didRetrieveToDoTasks(_ tasks: [TasksModel])
    //CoreData
     
}


protocol ToDoBoardRouterProtocol: class {
    //ToDoBoardPresenter -> ToDoBoardRouter
    func presentNewTask()
    
}

protocol ToDoBoardDataManagerOutput: class {
    // REMOTEDATAMANAGER -> INTERACTOR
   // func onPostsRetrieved(_ posts: [PostModel])
    func onError()
}

protocol ToDoBoardDataManagerInput: class {
    // INTERACTOR -> LOCALDATAMANAGER
    func retrieveTaskList() throws -> [TasksModel]
    func savePost(name: String) throws
    func deleteTask(id: String) throws
    func moveTaskToDoingList(id: String)
}
