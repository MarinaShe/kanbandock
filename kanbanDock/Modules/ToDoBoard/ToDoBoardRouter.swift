//
//  ToDoRouter.swift
//  kanbanDock
//
//  Created by Marina on 27.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

class ToDoBoardRouter {
    weak var viewController: ToDoBoardVC!
    weak var anableAction: UIAlertAction?
}

extension ToDoBoardRouter: ToDoBoardRouterProtocol{
    func presentNewTask(){
        let alertController = UIAlertController(title: NSLocalizedString("New task" , comment: ""), message: "", preferredStyle: .alert
        )
        alertController.addTextField(configurationHandler: {(textField: UITextField!) -> Void in
            textField.placeholder = NSLocalizedString("Enter task" , comment: "")
            textField.addTarget(self, action:  #selector(self.textChange), for: .editingChanged)
    })
        let saveAction = UIAlertAction(title: NSLocalizedString("Save" , comment: ""), style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            self.viewController.presenter?.saveNewTask(name: firstTextField.text!)
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel" , comment: ""), style: .cancel, handler: { alert -> Void in
            return
            //add new task to CoreData
            
        })
        saveAction.isEnabled = false
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.anableAction = saveAction
        viewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func textChange(_ sender: AnyObject) {
        if let textField = sender as? UITextField {
            self.anableAction?.isEnabled = !textField.text!.isEmpty
        }
    }
   
}
