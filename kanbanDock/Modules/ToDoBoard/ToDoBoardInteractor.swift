//
//  ToDointeractor.swift
//  kanbanDock
//
//  Created by Marina on 27.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ToDoBoardInteractor{
    weak var presenter: ToDoBoardInteractorOutput?
    //var localDatamanager: ToDoBoardDataManagerInput?
    var taskservice = TaskService()
}

extension ToDoBoardInteractor: ToDoBoardInteractorInput{

    func retrieveToDoList() {
        do {
            if let postList = try taskservice.retrieveTaskList(status: .toDo) as? [TasksModel] {
                if  postList.isEmpty {
                    presenter?.didRetrieveToDoTasks([])
                }else{
                    presenter?.didRetrieveToDoTasks(postList)
                }
            } else {
                presenter?.didRetrieveToDoTasks([])
            }
            
        } catch {
            presenter?.didRetrieveToDoTasks([])
        }
    }
    
    func addNewTaskinTodoList(name: String){
        do { try taskservice.savePost(name: name, dateFrom: nil, dateTo: nil, status: .toDo, active: true)
            retrieveToDoList()
        }
        catch {
            retrieveToDoList()
            return
        }
    }
    
    func deleteTask(id: String) {
        do { try taskservice.delTask(id: id)
            retrieveToDoList()
        }
        catch {
            retrieveToDoList()
            return
        }
    }
    
    func moveTaskToDoingList(id: String) {
        do { try taskservice.editTaskStatus(id: id, status: .doing)
            retrieveToDoList()
            NotificationCenter.default.post(name: NSNotification.Name("doing"), object: TaskStatus.doing)
        }
        catch {
            retrieveToDoList()
            print("ops")
            return
        }
        
    }
}
