//
//  ToDoPresenter.swift
//  kanbanDock
//
//  Created by Marina on 27.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation


class ToDoBoardPresenter {
    weak var view: ToDoBoardViewInput?
    var router: ToDoBoardRouterProtocol?
    var interactor: ToDoBoardInteractorInput?
    
}

extension ToDoBoardPresenter: ToDoBoardViewOutput{
    
    func viewDidLoad() {
        //view?.showLoading()
        view?.setupInitialView()
        //interactor?.retrieveToDoList()
    }
    
    func reloadData(){
        interactor?.retrieveToDoList()
    }

    func addButtonClicked(){
        router?.presentNewTask()
    }
    
    func saveNewTask(name:String) {
        interactor?.addNewTaskinTodoList(name: name)
    }
    
    func deleteTask(model: TasksModel) {
        interactor?.deleteTask(id: model.id)
    }
    
    func moveTaskToDoingList(task: TasksModel) {
        interactor?.moveTaskToDoingList(id: task.id)
    }
    
}

extension ToDoBoardPresenter: ToDoBoardInteractorOutput{
    func didRetrieveToDoTasks(_ tasks: [TasksModel]){
        //view?.hideLoading()
        view?.showToDoTasks(with: tasks)
    }
}
