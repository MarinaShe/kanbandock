//
//  ToDoConfigurator.swift
//  kanbanDock
//
//  Created by Marina on 27.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

class ToDoBoardConfigurator: DoDoBoardConfiguratorProtocol {
    
    func configure(with viewController: ToDoBoardVC) {
        let presenter = ToDoBoardPresenter()
        let interactor = ToDoBoardInteractor()
        let router = ToDoBoardRouter()
        //let localDatamanager = ToDoBoardDataManager()
        
        //connect
        viewController.presenter = presenter
        //viewController.router = router
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        
        interactor.presenter = presenter
        //interactor.localDatamanager = localDatamanager
        
        router.viewController = viewController
        
    }
    
}
