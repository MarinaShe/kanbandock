//
//  ToDoBoardTableViewCell.swift
//  kanbanDock
//
//  Created by Marina on 02.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

class ToDoTableViewCell: UITableViewCell, ModelTransferProtocol {

    @IBOutlet weak var nameLable: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func updateWithModel(_ model: Any?) {
        guard let task = model as? TasksModel else {
            return
        }
        self.nameLable.text = task.name
       
    }
    
    
}
