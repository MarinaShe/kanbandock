//
//  ToDoBoardDataManager.swift
//  kanbanDock
//
//  Created by Marina on 08.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import CoreData

class ToDoBoardDataManager: ToDoBoardDataManagerInput {
    
    
    var taskservice = TaskService()
    
    func retrieveTaskList() throws -> [TasksModel]{
        return try taskservice.retrieveTaskList(status: 1)
    }
    
    func savePost(name: String) throws {
       do { try taskservice.savePost(name: name, dateFrom: nil, dateTo: nil, status: 1, active: true)
        }
        catch {
            return
        } 
    }
    
    func deleteTask(id: String) throws {
        do { try taskservice.delTask(id: id)
        }
        catch {
            return
        }
    }
    
    func moveTaskToDoingList(id: String){
        do { try taskservice.editTaskStatus(id: id, status: 2)
        }
        catch {
            return
        }
    }
    
}
