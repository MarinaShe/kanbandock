//
//  ToDoBoardVC.swift
//  kanbanDock
//
//  Created by Marina on 26.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

class ToDoBoardVC: UIViewController {

    var presenter: ToDoBoardViewOutput?
    var configurator: DoDoBoardConfiguratorProtocol = ToDoBoardConfigurator()
    var tasksList: [TasksModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("to do")
        presenter?.reloadData()
    }
    
    @IBAction func newTask(_ sender: Any) {
        presenter?.addButtonClicked()
    }
    
    
    
}

extension ToDoBoardVC: ToDoBoardViewInput{
    func setupInitialView() {
        self.tableView?.register(ToDoTableViewCell.nib, forCellReuseIdentifier: ToDoTableViewCell.identifier)
        self.title = NSLocalizedString("To do list" , comment: "")
    }
    
    func showToDoTasks(with tasks:[TasksModel]) {
        tasksList = tasks
        tableView?.reloadData()
        self.navigationController?.tabBarItem.title = NSLocalizedString("To do" , comment: "") + "(\(tasksList.count))"
    }
}

extension ToDoBoardVC: UITableViewDataSource, UITableViewDelegate {
    func model(by path: IndexPath) -> TasksModel {
        return self.tasksList[path.row]
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasksList.count
    }
    
   /* func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(tasksList.count) " + NSLocalizedString("Task", comment: "")
    }
    */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ToDoTableViewCell.identifier, for: indexPath)
        if let updatebaleCell = cell as? ModelTransferProtocol {
            let model = self.model(by: indexPath)
            updatebaleCell.updateWithModel(model)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let model = self.model(by: indexPath) as TasksModel
            presenter?.deleteTask(model: model)
        
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    /*func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let toDoing = UITableViewRowAction(style: .normal, title: "Doing", handler: { (action, indexPath) in
            print("doing")
        })
        toDoing.backgroundColor = UIColor.yellow
        return [toDoing]
    }*/
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let toDoing = UIContextualAction(style: .normal, title: NSLocalizedString("Doing" , comment: ""), handler: { (ac: UIContextualAction,view: UIView,success:(Bool) -> Void)  in
            let model = self.model(by: indexPath) as TasksModel
            self.presenter?.moveTaskToDoingList(task: model)
            
        })
        toDoing.backgroundColor = UIColor.blue
        return UISwipeActionsConfiguration(actions: [toDoing])
    }
    
}
