//
//  DoingBoardDataManager.swift
//  kanbanDock
//
//  Created by Marina on 09.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import CoreData

class DoingBoardDataManager: DoingBoardDataManagerInput {
    var taskservice = TaskService()
    
    func retrieveTaskList() throws -> [TasksModel]{
        return try taskservice.retrieveTaskList(status: 2)
    }
    
    func savePost(name: String) throws {
        do { try taskservice.savePost(name: name, dateFrom: nil, dateTo: nil, status: 1, active: true)
        }
        catch {
            return
        }
    }
    
    
}
