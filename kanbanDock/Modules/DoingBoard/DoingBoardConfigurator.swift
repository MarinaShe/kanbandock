//
//  DoingBoardConfigurator.swift
//  kanbanDock
//
//  Created by Marina on 09.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

class DoingBoardConfigurator: DoingBoardConfiguratorProtocol {
    
    func configure(with viewController: DoingBoardVC) {
        let presenter = DoingBoardPresenter()
        let interactor = DoingBoardInteractor()
        let router = DoingBoardRouter()
        //let localDatamanager = DoingBoardDataManager()
        
        //connect
        viewController.presenter = presenter
        //viewController.router = router
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        
        interactor.presenter = presenter
        //interactor.localDatamanager = localDatamanager
        
        router.viewController = viewController
        
    }
    
}
