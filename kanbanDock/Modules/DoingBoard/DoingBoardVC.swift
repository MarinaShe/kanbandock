//
//  DoingBoardVC.swift
//  kanbanDock
//
//  Created by Marina on 26.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

class DoingBoardVC: UIViewController {

    var presenter: DoingBoardViewOutput?
    var configurator: DoingBoardConfiguratorProtocol = DoingBoardConfigurator()
    var tasksList: [TasksModel] = []
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter?.viewDidLoad()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        presenter?.reloadData()
    }
    
   /* @objc func reloadTaskList(){
            presenter?.reloadData()
    }*/
    
}

extension DoingBoardVC: DoingBoardViewInput{
    func setupInitialView() {
        //view.backgroundColor = UIColor.white
        self.tableView?.register(DoingTableViewCell.nib, forCellReuseIdentifier: DoingTableViewCell.identifier)
        self.title = NSLocalizedString("Doing list" , comment: "")
    }
    
    func showToDoTasks(with tasks:[TasksModel]) {
        tasksList = tasks
        tableView?.reloadData()
        self.navigationController?.tabBarItem.title = NSLocalizedString("Doing" , comment: "") + " (\(tasksList.count))"
    }
}

extension DoingBoardVC: UITableViewDataSource, UITableViewDelegate {
    func model(by path: IndexPath) -> TasksModel {
        return self.tasksList[path.row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasksList.count
    }
    
   /* func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(tasksList.count) task"
    }
    */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DoingTableViewCell.identifier, for: indexPath)
        if let updatebaleCell = cell as? ModelTransferProtocol {
            let model = self.model(by: indexPath)
            updatebaleCell.updateWithModel(model)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let toDone = UIContextualAction(style: .normal, title: NSLocalizedString("Done" , comment: ""), handler: { (ac: UIContextualAction,view: UIView,success:(Bool) -> Void)  in
            let model = self.model(by: indexPath) as TasksModel
            self.presenter?.moveTaskToDoneList(task: model)
        })
        toDone.backgroundColor = UIColor.blue
        return UISwipeActionsConfiguration(actions: [toDone])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let toDoing = UIContextualAction(style: .normal, title: NSLocalizedString("To do" , comment: ""), handler: { (ac: UIContextualAction,view: UIView,success:(Bool) -> Void)  in
            let model = self.model(by: indexPath) as TasksModel
            self.presenter?.moveTaskToDoList(task: model)
        })
        toDoing.backgroundColor = UIColor.red
        return UISwipeActionsConfiguration(actions: [toDoing])
    
    }
}
