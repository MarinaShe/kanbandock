//
//  DoingBoardInteractor.swift
//  kanbanDock
//
//  Created by Marina on 09.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit


class DoingBoardInteractor{
    weak var presenter: DoingBoardInteractorOutput?
    var taskservice = TaskService()
}

extension DoingBoardInteractor: DoingBoardInteractorInput{
    
    
    
    func retrieveToDoList() {
        do {
            if let postList = try taskservice.retrieveTaskList(status: .doing) as? [TasksModel] {
                if  postList.isEmpty {
                    presenter?.didRetrieveToDoTasks([])
                }else{
                    presenter?.didRetrieveToDoTasks(postList)
                }
            } else {
                presenter?.didRetrieveToDoTasks([])
            }
            
        } catch {
            presenter?.didRetrieveToDoTasks([])
        }
    }
    
    
    func deleteTask(id: String) {
        do { try taskservice.delTask(id: id)
            retrieveToDoList()
        }
        catch {
            retrieveToDoList()
            return
        }
    }
    
   func moveTaskToDoneList(id: String) {
        do { try taskservice.editTaskStatus(id: id, status: .done)
            retrieveToDoList()
            NotificationCenter.default.post(name: NSNotification.Name("done"), object: TaskStatus.done)
        }
        catch {
            retrieveToDoList()
            return
        }
    
    }
    
    func moveTaskToDoList(id: String){
        do { try taskservice.editTaskStatus(id: id, status: .toDo)
            retrieveToDoList()
            NotificationCenter.default.post(name: NSNotification.Name("toDo"), object: TaskStatus.toDo)
        }
        catch {
            //retrieveToDoList()
            return
        }
        
    }
}
