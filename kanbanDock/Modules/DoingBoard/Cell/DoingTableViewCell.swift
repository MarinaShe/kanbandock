//
//  DoingTableViewCell.swift
//  kanbanDock
//
//  Created by Marina on 09.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

class DoingTableViewCell: UITableViewCell, ModelTransferProtocol {
    
    
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var timeLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateWithModel(_ model: Any?) {
        guard let task = model as? TasksModel else {
            return
        }
        self.nameLable.text = task.name
        self.timeLable.text = String(task.dateFrom.dateDifferenceInStr(to: Date()))
    }
}
