//
//  DoingBoardPresenter.swift
//  kanbanDock
//
//  Created by Marina on 09.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
class DoingBoardPresenter {
    weak var view: DoingBoardViewInput?
    var router: DoingBoardRouterProtocol?
    var interactor: DoingBoardInteractorInput?
    
}

extension DoingBoardPresenter: DoingBoardViewOutput{
       
    
    func viewDidLoad() {
        //view?.showLoading()
        view?.setupInitialView()
        //interactor?.retrieveToDoList()
    }
    
    func reloadData(){
        interactor?.retrieveToDoList()
    }
    
    
    func moveTaskToDoneList(task: TasksModel){
        interactor?.moveTaskToDoneList(id: task.id)
    }
    
    func moveTaskToDoList(task: TasksModel){
        interactor?.moveTaskToDoList(id: task.id)
    }
    
    
}

extension DoingBoardPresenter: DoingBoardInteractorOutput{
    func didRetrieveToDoTasks(_ tasks: [TasksModel]){
        //view?.hideLoading()
        view?.showToDoTasks(with: tasks)
    }
}
