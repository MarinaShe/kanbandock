//
//  DoingBoardProtocol.swift
//  kanbanDock
//
//  Created by Marina on 09.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

protocol DoingBoardConfiguratorProtocol: class {
    func configure(with viewController: DoingBoardVC)
}

protocol DoingBoardViewInput: class {
    var presenter: DoingBoardViewOutput? {get set}
    
    // Presenter -> View
    func setupInitialView()
    func showToDoTasks(with tasks:[TasksModel])
}

protocol DoingBoardViewOutput: class {
    // View -> Presenter
    func viewDidLoad()
    func moveTaskToDoneList(task: TasksModel)
    func moveTaskToDoList(task: TasksModel)
    func reloadData()
}

protocol DoingBoardInteractorInput: class {
    var presenter: DoingBoardInteractorOutput? { get set }
    
    //Presenter -> Interactor
    func retrieveToDoList()
    func deleteTask(id: String)
    func moveTaskToDoneList(id: String)
    func moveTaskToDoList(id: String)
}

protocol DoingBoardInteractorOutput: class {
    
    //Interactor -> Presenter
    func didRetrieveToDoTasks(_ tasks: [TasksModel])
    //CoreData
}


protocol DoingBoardRouterProtocol: class {
    //Presenter -> Router
    
    
}

protocol DoingBoardDataManagerInput: class {
    // INTERACTOR -> LOCALDATAMANAGER
    func retrieveTaskList() throws -> [TasksModel]
    func savePost(name: String) throws
}
