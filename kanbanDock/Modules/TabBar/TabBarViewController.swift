//
//  TabBarViewController.swift
//  kanbanDock
//
//  Created by Marina on 17.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    //var taskservice = TaskService()
    var configurator: TabBarConfiguratorProtocol = TabBarConfigurator()
    var presenter: TabBarViewOutput?
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter?.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
}

extension TabBarViewController: TabBarViewInput {
    
    func setupInitialView() {
        for item in self.tabBar.items! {
            item.title = presenter?.getTitleItem(itemTag: item.tag)
        }
    }
    
    func setItemTitle(tag: Int){
        self.tabBar.items![tag].title = presenter?.getTitleItem(itemTag: tag)
    }
    
    
}
