//
//  TabBarInteractor.swift
//  kanbanDock
//
//  Created by Marina on 19.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation

class TabBarInteractor: TabBarInteractorInput {
    weak var presenter: TabBarInteractorOutput?
    var taskservice = TaskService()
    
    func getTaskCount(status: TaskStatus) -> Int {
       return taskservice.countTasks(status: status)
    }
    
    func setNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadItem(_:)), name: NSNotification.Name("toDo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadItem(_:)), name: NSNotification.Name("doing"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadItem(_:)), name: NSNotification.Name("done"), object: nil)
    }
    
    @objc func reloadItem(_ notification: Notification) {
        if let status = notification.object as? TaskStatus {
                presenter?.setTitleItem(status: status)
        }
    }
}
