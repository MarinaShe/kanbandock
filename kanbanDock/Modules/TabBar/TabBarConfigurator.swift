//
//  TabBarConfigurator.swift
//  kanbanDock
//
//  Created by Marina on 19.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
class TabBarConfigurator: TabBarConfiguratorProtocol {
    
    
    
    func configure(with viewController: TabBarViewController) {
        let presenter = TabBarPresenter()
        let interactor = TabBarInteractor()
        let router = TabBarRouter()
        
        //connect
        viewController.presenter = presenter
        //viewController.router = router
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        
        interactor.presenter = presenter
       
        
        router.viewController = viewController
    }
    
    
}
