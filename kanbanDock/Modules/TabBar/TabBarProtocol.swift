//
//  TabBarProtocol.swift
//  kanbanDock
//
//  Created by Marina on 19.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
protocol TabBarConfiguratorProtocol: class {
    func configure(with viewController: TabBarViewController)
}

protocol TabBarViewInput: class {
    var presenter: TabBarViewOutput? {get set}
    
    // Presenter -> View
    func setupInitialView()
    func setItemTitle(tag: Int)
    //func reloadItem(_ notification: Notification)
}

protocol TabBarViewOutput: class {
    // View -> Presenter
    func viewDidLoad()
    func getTitleItem(itemTag: Int) -> String
}

protocol TabBarInteractorInput: class {
    var presenter: TabBarInteractorOutput? { get set }
    //Presenter -> Interactor
    func getTaskCount(status: TaskStatus) -> Int
    func setNotification()
}

protocol TabBarInteractorOutput: class {
    //Interactor -> Presenter
    func setTitleItem(status: TaskStatus) 
}


protocol TabBarRouterProtocol: class {
    //Presenter -> Router
    
}

protocol TabBarDataManagerInput: class {
    // INTERACTOR -> LOCALDATAMANAGER
    
}
