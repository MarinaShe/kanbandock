//
//  TabBarPresenter.swift
//  kanbanDock
//
//  Created by Marina on 19.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation

class TabBarPresenter {
    weak var view: TabBarViewInput?
    var router: TabBarRouterProtocol?
    var interactor: TabBarInteractorInput?
}

extension TabBarPresenter: TabBarViewOutput {
    func viewDidLoad() {
        view?.setupInitialView()
        interactor?.setNotification()
    }
    
    func getTitleItem(itemTag: Int) -> String{
        switch itemTag {
        case 0: return NSLocalizedString("To do" , comment: "") + " (\(interactor?.getTaskCount(status: .toDo) ?? 0))"
            
        case 1: return NSLocalizedString("Doing" , comment: "") + " (\(interactor?.getTaskCount(status: .doing) ?? 0))"
            
        case 2: return NSLocalizedString("Done" , comment: "") + " (\(interactor?.getTaskCount(status: .done) ?? 0))"
            
        default: return ""
        }
    }
    
    
    
}

extension TabBarPresenter: TabBarInteractorOutput {
    func setTitleItem(status: TaskStatus){
        let itemTag = Int(status.rawValue)
        view?.setItemTitle(tag: itemTag)
    }
}
