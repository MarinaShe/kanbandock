//
//  TaskViewController.swift
//  kanbanDock
//
//  Created by Marina on 02.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

class TaskDetailViewController: UIViewController {
    var presenter: TaskDetailPresenterProtocol?
    var configurator: TaskDetailConfiguratorProtocol = TaskDetailConfigurator()
    var tasksList: [TasksModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        // Do any additional setup after loading the view.
    }
}

extension TaskDetailViewController: TaskDetailViewProtocol{
    func setupInitialView() {
        //set cell
    }
    
    func showToDoTasks(with tasks:[TasksModel]) {
        tasksList = tasks
    }
}

