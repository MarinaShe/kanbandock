//
//  TaskProtocol.swift
//  kanbanDock
//
//  Created by Marina on 02.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

protocol TaskDetailConfiguratorProtocol: class {
    func configure(with viewController: TaskDetailViewController)
}

protocol TaskDetailViewProtocol: class {
    var presenter: TaskDetailPresenterProtocol? {get set}
    
    // ToDOBoardPresenter -> ToDoBoardView
    func setupInitialView()
    func showToDoTasks(with tasks:[TasksModel])
}

protocol TaskDetailPresenterProtocol: class {
    // ToDoBoardView -> ToDoBoardPresenter
    func configureView()
    
    //ToDoboardInteractor -> ToDoBoardPresenter
    func didRetrieveToDoTasks(_ tasks: [TasksModel])
}

protocol TaskDetailInteractorProtocol: class {
    //ToDoBoardPresenter -> ToDoboardInteractor
    
    
    //CoreData
}

protocol TaskDetailRouterProtocol: class {
    //ToDoBoardPresenter -> ToDoBoardRouter
}

protocol TaskDetailDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    // func onPostsRetrieved(_ posts: [PostModel])
    func onError()
}

protocol TaskDetailDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
    //func retrieveTaskList() throws -> [Tasks]
    func savePost(name: String, dateFrom: NSDate?, dateTo: NSDate?, status: Int32, active: Bool) throws
}
