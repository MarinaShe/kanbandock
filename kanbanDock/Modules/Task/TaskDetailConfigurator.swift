//
//  TaskConfigurator.swift
//  kanbanDock
//
//  Created by Marina on 02.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
class TaskDetailConfigurator: TaskDetailConfiguratorProtocol{
    
    func configure(with viewController: TaskDetailViewController) {
        let presenter = TaskDetailPresenter()
        let interactor = TaskDetailInteractor()
        let router = TaskDetailRouter()
        
        //connect
        viewController.presenter = presenter
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        
        interactor.presenter = presenter
        
        router.viewController = viewController
        
    }
    
}
