//
//  TaskPresenter.swift
//  kanbanDock
//
//  Created by Marina on 02.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
class TaskDetailPresenter {
    weak var view: TaskDetailViewProtocol?
    var router: TaskDetailRouterProtocol?
    var interactor: TaskDetailInteractorProtocol?
}

extension TaskDetailPresenter: TaskDetailPresenterProtocol{
    func configureView(){
        view?.setupInitialView()
    }
    
    func didRetrieveToDoTasks(_ tasks: [TasksModel]){
        //view?.hideLoading()
        view?.showToDoTasks(with: tasks)
    }
}
