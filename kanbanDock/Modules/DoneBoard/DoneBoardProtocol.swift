//
//  DoneBoardProtocol.swift
//  kanbanDock
//
//  Created by Marina on 15.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation


protocol DoneBoardConfiguratorProtocol: class {
    func configure(with viewController: DoneBoardVC)
}

protocol DoneBoardViewInput: class {
    var presenter: DoneBoardViewOutput? {get set}
    
    // Presenter -> View
    func setupInitialView()
    func showToDoTasks(with tasks:[TasksModel])
}

protocol DoneBoardViewOutput: class {
    // View -> Presenter
    func viewDidLoad()
    func moveTaskToDoingList(task: TasksModel)
    func reloadData()
    func deleteTask(model: TasksModel)
}

protocol DoneBoardInteractorInput: class {
    var presenter: DoneBoardInteractorOutput? { get set }
    
    //Presenter -> Interactor
    func retrieveToDoList()
    func deleteTask(id: String)
    func moveTaskToDoingList(id: String)
}

protocol DoneBoardInteractorOutput: class {
    
    //Interactor -> Presenter
    func didRetrieveToDoTasks(_ tasks: [TasksModel])
    //CoreData
}


protocol DoneBoardRouterProtocol: class {
    //Presenter -> Router
    
    
}

protocol DoneBoardDataManagerInput: class {
    // INTERACTOR -> LOCALDATAMANAGER
    func retrieveTaskList() throws -> [TasksModel]
    func savePost(name: String) throws
}
