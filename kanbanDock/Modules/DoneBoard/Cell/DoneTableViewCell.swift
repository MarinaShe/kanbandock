//
//  DoneTableViewCell.swift
//  kanbanDock
//
//  Created by Marina on 16.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

class DoneTableViewCell: UITableViewCell, ModelTransferProtocol {

    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var stopTimeLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWithModel(_ model: Any?) {
        guard let task = model as? TasksModel else {
            return
        }
        self.nameLable.text = task.name
        self.stopTimeLable.text = String(task.dateFrom.dateDifferenceInStr(to: task.dateTo))
    }
    
}
