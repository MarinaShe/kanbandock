//
//  DoneBoardVC.swift
//  kanbanDock
//
//  Created by Marina on 26.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

class DoneBoardVC: UIViewController {

    var presenter: DoneBoardViewOutput?
    var configurator: DoneBoardConfiguratorProtocol = DoneBoardConfigurator()
    var tasksList: [TasksModel] = []
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter?.reloadData()
    }
    
    /*@objc func reloadTaskList(){
        presenter?.reloadData()
    }*/

}

extension DoneBoardVC: DoneBoardViewInput{
    func setupInitialView() {
        self.tableView.register(DoneTableViewCell.nib, forCellReuseIdentifier: DoneTableViewCell.identifier)
        self.title = NSLocalizedString("Done list" , comment: "")
    }
    
    func showToDoTasks(with tasks:[TasksModel]) {
        tasksList = tasks
        tableView.reloadData()
        self.navigationController?.tabBarItem.title = NSLocalizedString("Done" , comment: "") + " (\(tasksList.count))"
    }
}

extension DoneBoardVC: UITableViewDataSource, UITableViewDelegate {
    func model(by path: IndexPath) -> TasksModel {
        return self.tasksList[path.row]
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasksList.count
    }

  /*  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(tasksList.count) task"
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DoneTableViewCell.identifier, for: indexPath)
        if let updatebaleCell = cell as? ModelTransferProtocol {
            let model = self.model(by: indexPath)
            updatebaleCell.updateWithModel(model)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let toDoing = UIContextualAction(style: .normal, title: NSLocalizedString("Doing" , comment: ""), handler: { (ac: UIContextualAction,view: UIView,success:(Bool) -> Void)  in
            let model = self.model(by: indexPath) as TasksModel
            self.presenter?.moveTaskToDoingList(task: model)
        })
        toDoing.backgroundColor = UIColor.orange
        return UISwipeActionsConfiguration(actions: [toDoing])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let toDone = UIContextualAction(style: .normal, title: NSLocalizedString("Delete" , comment: ""), handler: { (ac: UIContextualAction,view: UIView,success:(Bool) -> Void)  in
            let model = self.model(by: indexPath) as TasksModel
            self.presenter?.deleteTask(model: model)
            
        })
        toDone.backgroundColor = UIColor.red
        return UISwipeActionsConfiguration(actions: [toDone])
    }
    
    
}

