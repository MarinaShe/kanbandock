//
//  DoneBoardConfigurator.swift
//  kanbanDock
//
//  Created by Marina on 15.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
import UIKit

class DoneBoardConfigurator: DoneBoardConfiguratorProtocol {
    
    func configure(with viewController: DoneBoardVC) {
        let presenter = DoneBoardPresenter()
        let interactor = DoneBoardInteractor()
        let router = DoneBoardRouter()
        //let localDatamanager = DoingBoardDataManager()
        
        //connect
        viewController.presenter = presenter
        //viewController.router = router
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        
        interactor.presenter = presenter
        //interactor.localDatamanager = localDatamanager
        
        router.viewController = viewController
        
    }
    
}
