//
//  DoneBoardPresenter.swift
//  kanbanDock
//
//  Created by Marina on 15.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation
class DoneBoardPresenter {
    weak var view: DoneBoardViewInput?
    var router: DoneBoardRouterProtocol?
    var interactor: DoneBoardInteractorInput?
    
}

extension DoneBoardPresenter: DoneBoardViewOutput{
    
    func viewDidLoad() {
        //view?.showLoading()
        view?.setupInitialView()
        //interactor?.retrieveToDoList()
    }
    
    func reloadData(){
        interactor?.retrieveToDoList()
    }
    
    
    func moveTaskToDoingList(task: TasksModel){
        interactor?.moveTaskToDoingList(id: task.id)
    }
    
    func deleteTask(model: TasksModel) {
        interactor?.deleteTask(id: model.id)
    }
    
}

extension DoneBoardPresenter: DoneBoardInteractorOutput{
    func didRetrieveToDoTasks(_ tasks: [TasksModel]){
        //view?.hideLoading()
        view?.showToDoTasks(with: tasks)
    }
}
