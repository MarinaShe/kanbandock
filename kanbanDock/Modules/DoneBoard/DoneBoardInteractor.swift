//
//  DoneBoardInteractor.swift
//  kanbanDock
//
//  Created by Marina on 15.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation


class DoneBoardInteractor{
    weak var presenter: DoneBoardInteractorOutput?
    var taskservice = TaskService()
}

extension DoneBoardInteractor: DoneBoardInteractorInput{
    
    
    
    func retrieveToDoList() {
        do {
            if let postList = try taskservice.retrieveTaskList(status: .done) as? [TasksModel] {
                if  postList.isEmpty {
                    presenter?.didRetrieveToDoTasks([])
                }else{
                    presenter?.didRetrieveToDoTasks(postList)
                }
            } else {
                presenter?.didRetrieveToDoTasks([])
            }
            
        } catch {
            presenter?.didRetrieveToDoTasks([])
        }
    }
    
    
    func deleteTask(id: String) {
        do { try taskservice.delTask(id: id)
            retrieveToDoList()
        }
        catch {
            retrieveToDoList()
            return
        }
    }
    
    func moveTaskToDoingList(id: String) {
        do { try taskservice.editTaskStatus(id: id, status: .doing)
            retrieveToDoList()
            NotificationCenter.default.post(name: NSNotification.Name("doing"), object: TaskStatus.doing)
        }
        catch {
            retrieveToDoList()
            return
        }
        
    }
}
