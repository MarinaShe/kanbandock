//
//  Board.swift
//  kanbanDock
//
//  Created by Marina on 26.03.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation

class Board: Codable {
    var title: String
    var items: [String]
    
    init(title: String, items: [String]) {
        self.title = title
        self.items = items
    }
}

