//
//  UITableViewCellExtensions.swift
//  kanbanDock
//
//  Created by AlexT on 05/04/2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
