//
//  DateExtensions.swift
//  kanbanDock
//
//  Created by Marina on 16.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation

extension Date {
    func days(to secondDate: Date, calendar: Calendar = Calendar.current) -> Int {
        return calendar.dateComponents([.day], from: self, to: secondDate).day!
    }
    
    func hour(to secondDate: Date, calendar: Calendar = Calendar.current) -> Int {
        return (calendar.dateComponents([.hour], from: self, to: secondDate).hour!) % 24
    }
    
    func dateDifferenceInStr(to secondDate: Date) -> String {
        let dayDif = self.days(to: secondDate)
        let hDif = self.hour(to: secondDate)
        if dayDif > 0 {
            return String(dayDif) + NSLocalizedString("d" , comment: "") + String(hDif) + NSLocalizedString("h" , comment: "")
        } else {
          return String(hDif) + NSLocalizedString("h" , comment: "")
        }
    }
}
