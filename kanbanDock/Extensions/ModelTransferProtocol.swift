//
//  ModelTransferProtocol.swift
//  kanbanDock
//
//  Created by AlexT on 05/04/2019.
//  Copyright © 2019 Marina. All rights reserved.
//

public protocol ModelTransferProtocol {
    func updateWithModel(_ model: Any?)
}
