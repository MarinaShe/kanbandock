//
//  TaskServiceInterface.swift
//  kanbanDock
//
//  Created by Marina on 05.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation

protocol TaskServiceInterface {
    func retrieveTaskList(status: TaskStatus) throws -> [TasksModel]
    func savePost(name: String, dateFrom: NSDate?, dateTo: NSDate?, status: TaskStatus, active: Bool) throws
    func delTask(id: String) throws
    func editTaskStatus(id: String, status: TaskStatus) throws
    func countTasks(status: TaskStatus) -> Int
}
