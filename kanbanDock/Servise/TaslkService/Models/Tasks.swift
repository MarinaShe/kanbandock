//
//  Tasks+CoreDataProperties.swift
//  kanbanDock
//
//  Created by Marina on 01.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//
//

import Foundation
import CoreData

@objc public enum TaskStatus : Int16 {
    case toDo = 0
    case doing = 1
    case done = 2

    var title : String {
        var state:String = ""
        switch self {
        case .toDo: state = NSLocalizedString("To do", comment: "")
        case .doing: state = NSLocalizedString("Doing", comment: "")
        case .done: state = NSLocalizedString("Done", comment: "")
        }
        return state
    }
 
}

@objc(Tasks)
public class Tasks: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tasks> {
        return NSFetchRequest<Tasks>(entityName: "Tasks")
    }
    
    @NSManaged public var name: String?
    @NSManaged public var dateFrom: NSDate?
    @NSManaged public var dateTo: NSDate?
    @NSManaged public var status: TaskStatus
    @NSManaged public var active: Bool
    @NSManaged public var id: String

}






