//
//  ToDoBoardDataManager.swift
//  kanbanDock
//
//  Created by Marina on 01.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import CoreData

class TaskService:TaskServiceInterface {
   
    
    
    func retrieveTaskList(status: TaskStatus) throws -> [TasksModel]  {
        
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        let request: NSFetchRequest<Tasks> = NSFetchRequest(entityName: String(describing: Tasks.self))
        let predicate = NSPredicate(format: "status == %d", status.rawValue)
        request.predicate = predicate
        
        let result = try managedOC.fetch(request)
        let postModelList = result.map() {
            return TasksModel(name: $0.name!, dateFrom: $0.dateFrom as Date? ?? Date() , dateTo: $0.dateTo as Date? ?? Date(), status: $0.status, active: $0.active,  id: $0.id)
        }
        return postModelList
    }
    
    func savePost(name: String, dateFrom: NSDate?, dateTo: NSDate?, status: TaskStatus, active: Bool) throws {
        guard let managedOC = CoreDataStore.backgroundContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        guard let newTask = NSEntityDescription.entity(forEntityName: "Tasks",
                                                       in: managedOC)
            else {throw PersistenceError.couldNotSaveObject}
        
            let task = Tasks(entity: newTask, insertInto: managedOC)
            task.name = name
            task.dateFrom = dateFrom
            task.dateTo =  dateTo
            task.status = status
            task.active = active
            let randomString = UUID().uuidString
            task.id = randomString
        
        do {try managedOC.save()}
        catch { throw PersistenceError.couldNotSaveObject}
  
    }
    
    func delTask(id: String) throws {
        guard let managedOC = CoreDataStore.backgroundContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        let request: NSFetchRequest<Tasks> = NSFetchRequest(entityName: String(describing: Tasks.self))
        let predicate = NSPredicate(format: "id == %@", id)
        request.predicate = predicate
        
        if let result = try? managedOC.fetch(request) {
            managedOC.delete(result.first!)
        }
        do {try managedOC.save()}
        catch { throw PersistenceError.couldNotSaveObject}
    }
    
    func editTaskStatus(id: String, status: TaskStatus)  throws {
        guard let managedOC = CoreDataStore.backgroundContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        let request: NSFetchRequest<Tasks> = NSFetchRequest(entityName: String(describing: Tasks.self))
        let predicate = NSPredicate(format: "id == %@", id)
        request.predicate = predicate
        
        if let result = try? managedOC.fetch(request) {
            let task = result.first
            let oldstatus = task?.status
            task?.status = status
            //set date
            if status == .doing && oldstatus == .toDo {
                task?.dateFrom = Date() as NSDate
            }
            if status == .done {
                task?.dateTo = Date() as NSDate
            }
        }
        do {try managedOC.save()}
        catch { throw PersistenceError.couldNotSaveObject}
    }
   
    func countTasks(status: TaskStatus) -> Int {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            return 0
        }
        
        let request: NSFetchRequest<Tasks> = NSFetchRequest(entityName: String(describing: Tasks.self))
        let predicate = NSPredicate(format: "status == %d", status.rawValue)
        request.predicate = predicate
        
        do {let result =  try managedOC.fetch(request)
            return result.count
        }
        catch{
            return 0
        }
        
        
    }
    
}
