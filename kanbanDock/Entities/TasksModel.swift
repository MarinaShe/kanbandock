//
//  TasksModel.swift
//  kanbanDock
//
//  Created by Marina on 02.04.2019.
//  Copyright © 2019 Marina. All rights reserved.
//

import Foundation

/*protocol TasksModel: class {
    var name: String? {get}
    var dateFrom: NSDate? {get set}
    var dateTo: NSDate? {get set}
    var status: Int32 {get set}
    var active: Bool {get set}
    var id: String {get set}
    
    //func taskToModel(name: String?, dateFrom: NSDate?, dateTo: NSDate?, status: Int32, active: Bool)
    
}*/

struct TasksModel {
    var name = ""
    var dateFrom = Date()
    var dateTo = Date()
    var status = TaskStatus.toDo
    var active = true
    var id = ""
}
